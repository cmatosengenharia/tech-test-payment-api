﻿using Domain.Enum;

namespace Application.DTOs
{
    public class ObterVendaDto
    {
        public Guid IdentificadorPedido { get; set; }
        public int IdentificadorVendedor { get; set; }
        public string Nome { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Telefone { get; set; } = string.Empty;
        public DateTime DataVenda { get; set; }
        public string StatusDoPedido { get; set; } = string.Empty;
        public List<ItemVendidoDto> Items { get; set; } = new List<ItemVendidoDto>();
    }
}