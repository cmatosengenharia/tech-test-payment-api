﻿using Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Application.DTOs
{
    public class RegistrarVendaDto
    {
        [Required(ErrorMessage = "O identificador do vendedor é obrigatório.")]
        [Range(1, int.MaxValue, ErrorMessage = "O identificador do vendedor deve ser maior que zero.")]
        public int IdentificadorVendedor { get; set; }

        [Required(ErrorMessage = "A data da venda é obrigatória.")]
        public DateTime DataVenda { get; set; }

        [Required(ErrorMessage = "A lista de itens vendidos não pode ser vazia.")]
        [MinLength(1, ErrorMessage = "Deve haver pelo menos um item na venda.")]
        public List<ItemVendidoDto> ItensVendidos { get; set; } = new List<ItemVendidoDto>();
    }
}