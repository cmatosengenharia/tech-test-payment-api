﻿using Domain.Enum;

namespace Application.DTOs
{
    public class AtualizarStatusVendaDto
    {
        public SaleStatus StatusDoPedido { get; set; }
    }
}