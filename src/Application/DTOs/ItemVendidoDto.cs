﻿using Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Application.DTOs
{
    public class ItemVendidoDto
    {
        [Required(ErrorMessage = "O identificador do produto é obrigatório.")]
        [Range(1, int.MaxValue, ErrorMessage = "O identificador do produto deve ser um número positivo maior que zero.")]
        public int IdentificadorProduto { get; set; }

        [Required(ErrorMessage = "O preço do produto é obrigatório.")]
        [Range(0.01, double.MaxValue, ErrorMessage = "O preço do produto deve ser maior que zero.")]
        public decimal Preco { get; set; }

        [Required(ErrorMessage = "O nome do produto é obrigatório.")]
        [StringLength(100, ErrorMessage = "O nome do produto não pode exceder 100 caracteres.")]
        public string NomeProduto { get; set; } = string.Empty;

        [Required(ErrorMessage = "A quantidade do produto é obrigatória.")]
        [Range(1, int.MaxValue, ErrorMessage = "A quantidade do produto deve ser maior que zero.")]
        public int Quantidade { get; set; }

        public decimal Total { get => Preco * Quantidade; }
    }
}