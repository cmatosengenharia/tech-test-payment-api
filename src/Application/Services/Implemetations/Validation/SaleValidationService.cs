﻿using Application.DTOs;
using Application.Services.Interface.Validation;
using Domain.Entities;
using Domain.Enum;
using Domain.Repositorys.Interfaces;

namespace Application.Services.Implemetations.Validation
{
    public class SaleValidationService : ISaleValidationService
    {
        private readonly ISellerRepository _sellerRepository;
        private readonly ISaleRepository _saleRepository;
        public SaleValidationService(ISellerRepository sellerRepository,
            ISaleRepository saleRepository)
        {
            _sellerRepository = sellerRepository;
            _saleRepository = saleRepository;
        }

        public void ValidateSaleItems(List<ItemVendidoDto> items)
        {
            if (items == null || items.Count == 0)
                throw new InvalidOperationException("A venda deve conter pelo menos um item.");

            if (items.Exists(item => item.IdentificadorProduto == 0))
                throw new InvalidOperationException("Todos os produtos devem ter um identificador válido.");
        }

        public void ValidateSeller(int sellerId)
        {
            var seller = _sellerRepository.GetByIdAsync(sellerId);
            if (sellerId == 0 || seller == null)
                throw new InvalidOperationException("Vendedor não encontrado.");
        }

        public Sale ValidateSale(int id) 
        {
            var sale = _saleRepository.GetByIdAsync(id);
            if (sale == null)
            {
                throw new InvalidOperationException("Venda não encontrada.");
            }
            else 
            {
                return sale;
            }    
        }

        public bool IsStatusValid(SaleStatus newStatus)
        {
            bool isValid = newStatus == SaleStatus.PagamentoAprovado ||
                           newStatus == SaleStatus.EnviadoParaTransportadora ||
                           newStatus == SaleStatus.Entregue ||
                           newStatus == SaleStatus.Cancelada;

      
            if (!isValid)
            {
                throw new InvalidOperationException("Status não permitido para atualização");
            }

            return isValid;
        }

        public bool IsTransitionAllowed(SaleStatus currentStatus, SaleStatus newStatus)
        {
            bool isAllowed = currentStatus switch
            {
                SaleStatus.AguardandoPagamento => newStatus == SaleStatus.PagamentoAprovado || newStatus == SaleStatus.Cancelada,
                SaleStatus.PagamentoAprovado => newStatus == SaleStatus.EnviadoParaTransportadora || newStatus == SaleStatus.Cancelada,
                SaleStatus.EnviadoParaTransportadora => newStatus == SaleStatus.Entregue,
                _ => false
            };

            if (!isAllowed)
            {
                throw new InvalidOperationException("Transição de status não permitida");
            }

            return isAllowed;
        }

    }
}