﻿using Application.DTOs;
using Application.Services.Interface;
using Application.Services.Interface.Validation;
using Common.Utilities.Enum;
using Domain.Entities;
using Domain.Enum;
using Domain.Repositorys.Interfaces;

namespace Application.Services.Implemetations
{
    public class SalesService : ISalesService
    {
        private readonly ISaleRepository _saleRepository;
        private readonly ISellerRepository _sellerRepository;
        private readonly ISaleValidationService _saleValidationService;
        public SalesService(ISaleRepository saleRepository,
            ISaleValidationService saleValidationService,
            ISellerRepository sellerRepository)
        {
            _saleRepository = saleRepository;
            _saleValidationService = saleValidationService;
            _sellerRepository = sellerRepository;
        }

        public (string, int) RegisterSale(RegistrarVendaDto dto)
        {
            _saleValidationService.ValidateSaleItems(dto.ItensVendidos);

            _saleValidationService.ValidateSeller(dto.IdentificadorVendedor);

            var sale = new Sale
            {
                Identifier = Guid.NewGuid(),
                SellerId = dto.IdentificadorVendedor,
                Date = dto.DataVenda,
                Status = SaleStatus.AguardandoPagamento,
                Seller = _sellerRepository.GetByIdAsync(dto.IdentificadorVendedor),
                Items = dto.ItensVendidos.ConvertAll(item => new SaleItem
                {
                    ProductId = item.IdentificadorProduto,
                    UnitPrice = item.Preco,
                    Quantity = item.Quantidade,
                    ProductNome = item.NomeProduto
                })
            };

            var saleResult = _saleRepository.AddAsync(sale);

            return (sale.Status.GetDescription(), saleResult.Id);
        }
        public ObterVendaDto GetSaleById(int id)
        {
            var sale = _saleValidationService.ValidateSale(id);

            return new ObterVendaDto
            {
                IdentificadorPedido = sale.Identifier,
                IdentificadorVendedor = sale.SellerId,
                DataVenda = sale.Date,
                StatusDoPedido = sale.Status.GetDescription(),
                Nome = sale.Seller.Name,
                Email = sale.Seller.Email,
                Telefone = sale.Seller.PhoneNumber,                
                Items = sale.Items.ConvertAll(item => new ItemVendidoDto
                {
                    IdentificadorProduto = item.ProductId,
                    Preco = item.UnitPrice,
                    Quantidade = item.Quantity,
                    NomeProduto = item.ProductNome
                })
            };
        }
        public bool UpdateStatusSale(int id, SaleStatus newStatus)
        {
            var sale = _saleValidationService.ValidateSale(id);

            _saleValidationService.IsStatusValid(newStatus);

            _saleValidationService.IsTransitionAllowed(sale.Status, newStatus);

            sale.Status = newStatus;

            _saleRepository.UpdateAsync(sale);

            return true;
        }
    }
}