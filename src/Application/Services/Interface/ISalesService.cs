﻿using Application.DTOs;
using Domain.Enum;

namespace Application.Services.Interface
{
    public interface ISalesService
    {
        (string, int) RegisterSale(RegistrarVendaDto dto);
        ObterVendaDto GetSaleById(int id);
        bool UpdateStatusSale(int id, SaleStatus newStatus);
    }
}
