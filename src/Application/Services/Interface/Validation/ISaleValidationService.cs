﻿using Application.DTOs;
using Domain.Entities;
using Domain.Enum;

namespace Application.Services.Interface.Validation
{
    public interface ISaleValidationService
    {
        void ValidateSaleItems(List<ItemVendidoDto> items);
        void ValidateSeller(int sellerId);
        Sale ValidateSale(int id);
        bool IsStatusValid(SaleStatus newStatus);
        bool IsTransitionAllowed(SaleStatus currentStatus, SaleStatus newStatus);
    }
}