﻿using Application.DTOs;
using Domain.Entities;
using Domain.Repositorys.Interfaces;
using Moq;

namespace UnitTests.ApplicationSetup
{
    public class MockSetup
    {
        private readonly Mock<ISellerRepository> _mockSellerRepository;
        private readonly Mock<ISaleRepository> _mockSaleRepository;

        public MockSetup(Mock<ISellerRepository> mockSellerRepository,
            Mock<ISaleRepository> mockSaleRepository)
        {
            _mockSellerRepository = mockSellerRepository;
            _mockSaleRepository = mockSaleRepository;
        }

        public void SetupSellers()
        {
            var sellers = new List<Seller>
                          {
                              CreateSeller(1, "Geraldo", "geraldo1950@teste.com", "31 9 9999-4444"),
                              CreateSeller(2, "Joaquim", "joaquim1951@example.com", "31 9 9999-4445"),
                              CreateSeller(3, "Ferdinanda", "ferdinanda1960@example.com", "31 9 9999-4446")
                          };

            foreach (var seller in sellers)
            {
                _mockSellerRepository.Setup(r => r.GetByIdAsync(seller.Id)).Returns(seller);
            }
        }

        public void SetupSaleRepository()
        {
            _mockSaleRepository.Setup(repo => repo.AddAsync(It.IsAny<Sale>()))
                               .Returns((Sale sale) =>
                               {
                                   if (sale.Id == 0)
                                   {
                                       sale.Id = 1;
                                   }
                                   return sale;
                               });
        }

        public static Seller CreateSeller(int id, string name, string email, string phoneNumber)
        {
            return new Seller
            {
                Id = id,
                Name = name,
                Email = email,
                PhoneNumber = phoneNumber
            };
        }

        public static SaleItem CreateSaleItem() 
        {
            return new SaleItem 
            { 
                ProductId = 1,
                UnitPrice = 100,
                Quantity = 2,
                ProductNome = "Produto A" 
            };
        }

        public static ItemVendidoDto CreateItemVendidoDto()
        {
            return new ItemVendidoDto
            {
                IdentificadorProduto = 1,
                Preco = 100,
                NomeProduto = "Produto A",
                Quantidade = 2
            };
        }
    }
}