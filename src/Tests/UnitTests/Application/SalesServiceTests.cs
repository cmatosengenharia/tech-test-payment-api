using Application.DTOs;
using Application.Services.Implemetations;
using Application.Services.Interface.Validation;
using Common.Utilities.Enum;
using Domain.Entities;
using Domain.Enum;
using Domain.Repositorys.Interfaces;
using Moq;
using UnitTests.ApplicationSetup;

namespace UnitTests.Application
{
    public class SalesServiceTests
    {
        private readonly Mock<ISaleRepository> _mockSaleRepository = new();
        private readonly Mock<ISellerRepository> _mockSellerRepository = new();
        private readonly Mock<ISaleValidationService> _mockSaleValidationService = new();
        private readonly SalesService _salesService;
        private readonly MockSetup _mockSetup;


        public SalesServiceTests()
        {
            _salesService = new SalesService(_mockSaleRepository.Object, _mockSaleValidationService.Object, _mockSellerRepository.Object);
            _mockSetup = new MockSetup(_mockSellerRepository, _mockSaleRepository);
            _mockSetup.SetupSellers();
            _mockSetup.SetupSaleRepository();
        }

        [Fact]
        public void RegisterSale_ValidInput_ReturnsCorrectStatusAndId()
        {
            //Input usu�rio Arrange
            var dto = new RegistrarVendaDto
            {
                IdentificadorVendedor = 1,
                DataVenda = DateTime.Now,
                ItensVendidos = new List<ItemVendidoDto>
                                {
                                    MockSetup.CreateItemVendidoDto()
                                }
            };


            _mockSaleValidationService.Setup(s => s.ValidateSaleItems(It.IsAny<List<ItemVendidoDto>>()));
            _mockSaleValidationService.Setup(s => s.ValidateSeller(It.IsAny<int>()));

            // Act
            var result = _salesService.RegisterSale(dto);

            // Assert
            Assert.NotNull(result.Item1);
            Assert.NotEqual(0, result.Item2);
            Assert.Equal(SaleStatus.AguardandoPagamento.GetDescription(), result.Item1);
            Assert.Equal(1, result.Item2);
            _mockSaleRepository.Verify(r => r.AddAsync(It.IsAny<Sale>()), Times.Once);
            _mockSaleValidationService.Verify();
        }

        [Fact]
        public void GetSaleById_ValidId_ReturnsSaleDto()
        {
            //Input usu�rio Arrange
            int saleId = 1;
            var mockSale = new Sale
            {
                Identifier = Guid.NewGuid(),
                SellerId = 1,
                Date = DateTime.Now,
                Status = SaleStatus.AguardandoPagamento,
                Seller = new Seller { Name = "Geraldo", Email = "geraldo1950@teste.com", PhoneNumber = "31 9 9999-4444" },
                Items = new List<SaleItem>
                            {
                                MockSetup.CreateSaleItem()
                            }
            };

            _mockSaleValidationService.Setup(s => s.ValidateSale(saleId)).Returns(mockSale);

            // Act
            ObterVendaDto result = _salesService.GetSaleById(saleId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(mockSale.Identifier, result.IdentificadorPedido);
            Assert.Equal(mockSale.SellerId, result.IdentificadorVendedor);
            Assert.Equal(mockSale.Date, result.DataVenda);
            Assert.Equal(mockSale.Status.GetDescription(), result.StatusDoPedido);
            Assert.Equal(mockSale.Seller.Name, result.Nome);
            Assert.Equal(mockSale.Seller.Email, result.Email);
            Assert.Equal(mockSale.Seller.PhoneNumber, result.Telefone);
            Assert.Single(result.Items);
            Assert.Equal(mockSale.Items[0].ProductId, result.Items[0].IdentificadorProduto);
            Assert.Equal(mockSale.Items[0].UnitPrice, result.Items[0].Preco);
            Assert.Equal(mockSale.Items[0].Quantity, result.Items[0].Quantidade);
            Assert.Equal(mockSale.Items[0].ProductNome, result.Items[0].NomeProduto);

            _mockSaleValidationService.Verify();
        }

        [Fact]
        public void UpdateStatusSale_ValidTransition_ReturnsTrue()
        {
            //Input usu�rio Arrange
            int saleId = 1;
            var initialStatus = SaleStatus.AguardandoPagamento;
            var newStatus = SaleStatus.PagamentoAprovado;
            var sale = new Sale { Id = saleId, Status = initialStatus };

            _mockSaleValidationService.Setup(s => s.ValidateSale(saleId)).Returns(sale);
            _mockSaleValidationService.Setup(s => s.IsStatusValid(newStatus)).Verifiable();
            _mockSaleValidationService.Setup(s => s.IsTransitionAllowed(initialStatus, newStatus)).Verifiable();
            _mockSaleRepository.Setup(r => r.UpdateAsync(sale));

            // Act
            var result = _salesService.UpdateStatusSale(saleId, newStatus);

            // Assert
            Assert.True(result);
            Assert.Equal(newStatus, sale.Status);
            _mockSaleRepository.Verify(r => r.UpdateAsync(sale), Times.Once);
            _mockSaleValidationService.Verify();
        }
    }
}