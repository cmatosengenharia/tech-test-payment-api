﻿using Domain.Enum;
using System.ComponentModel;
using System.Reflection;

namespace Common.Utilities.Enum
{
    public static class EnumExtensions
    {
        public static string GetDescription(this SaleStatus value)
        {
            #pragma warning disable CS8600
            FieldInfo fi = value.GetType().GetField(value.ToString());
            #pragma warning restore CS8600

            if (fi != null)
            {
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
            }

            return value.ToString();
        }
    }
}