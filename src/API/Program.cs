using API.Middlewares;
using Infrastructure.DependencyInjection;
using Infrastructure.MemoryCache.Extensions;
using Microsoft.Extensions.Caching.Memory;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddInfrastructureServices();

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
    {
        Title = "API Teste t�cnico Pottencial",
        Version = "v1",
        Description = "API REST que realiza transa��es de venda.",
        Contact = new Microsoft.OpenApi.Models.OpenApiContact
        {
            Name = "C�ssio Matos",
            Email = "cmatosengenharia@gmail.com",
            Url = new Uri($"https://github.com/cmatosengenharia"),
        }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Teste t�cnico Pottencial v1");
        c.InjectStylesheet("/css/swagger-custom.css");
        c.InjectJavascript("/js/custom-swagger.js");
    });
}

app.UseStaticFiles();

app.UseRouting();

app.UseMiddleware<ErrorLoggingMiddleware>();

var cache = app.Services.GetRequiredService<IMemoryCache>();
cache.InitializeSellerCache();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();