﻿using Application.DTOs;
using Application.Services.Interface;
using Common.Utilities.Enum;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly ISalesService _salesService;

        public VendaController(ISalesService salesService)
        {
            _salesService = salesService;
        }

        /// <summary>
        /// - Rota registra uma nova venda.
        /// - Vendendores cadastrados no sistema.
        /// - Identificador Vendedor: 1 | Nome: Geraldo | Email: geraldo1950@teste.com | Tel: 31 9 99994444
        /// - Identificador Vendedor: 2 | Nome: Joaquim | Email: joaquim1951@example.com | Tel: 31 9 99994445
        /// - Identificador Vendedor: 3 | Nome: Ferdinanda | Email: ferdinanda1960@example.com | Tel: 31 9 99994446
        /// </summary>
        /// <param name="venda">Dados da venda - Informe o Identificador Vendedor com base nos vendedores que já existem. </param>
        /// <returns>Status da operação</returns>
        /// <response code="200">Retorna os status da operação.</response>
        /// <response code="500">Retorna uma mensagem de erro se ocorrer um erro ao processar a solicitação.</response>
        [HttpPost]
        public IActionResult RegistrarVenda([FromBody] RegistrarVendaDto venda)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var (statusVenda, vendaId) = _salesService.RegisterSale(venda);

                return Ok(new { StatusVenda = statusVenda , VendaId = vendaId });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Recupera os detalhes de uma venda específica pelo ID.
        /// </summary>
        /// <param name="id">O ID da venda que será recuperada. É gerado no retorno da requisição acima.</param>
        /// <returns>Retorna os detalhes da venda se encontrada; caso contrário, retorna uma mensagem de erro.</returns>
        /// <response code="200">Retorna os detalhes da venda encontrada.</response>
        /// <response code="500">Retorna uma mensagem de erro se ocorrer um erro ao processar a solicitação.</response>
        [HttpGet("{id}")]
        public IActionResult ObterVenda(int id)
        {
            try
            {
                var sale = _salesService.GetSaleById(id);

                return Ok(sale);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// - Atualiza o status de uma venda específica.
        /// - Permite que o status da venda seja atualizado para um dos seguintes valores:
        /// - PagamentoAprovado statusID: 1 | Indica que o pagamento da venda foi aprovado.
        /// - EnviadoParaTransportadora statusID: 2 | Indica que a venda foi enviada para a transportadora.
        /// - Entregue statusID: 3 | Indica que a venda foi entregue ao cliente.
        /// - Cancelada statusID: 4 | Indica que a venda foi cancelada.
        /// </summary>
        /// <param name="id">O ID da venda cujo status será atualizado.</param>
        /// <param name="atualizacaoStatusVenda">Qual o novo statusID que deseja atualizar a venda?</param>
        /// <returns>Retorna o resultado da operação de atualização.</returns>
        /// <response code="200">Retorna verdadeiro se o status da venda foi atualizado com sucesso.</response>
        /// <response code="500">Retorna uma mensagem de erro se ocorrer um erro ao processar a solicitação.</response>
        [HttpPut("{id}")]
        public IActionResult AtualizacaoStatusVenda(int id, [FromBody] AtualizarStatusVendaDto atualizacaoStatusVenda)
        {
            try
            {
                var result = _salesService.UpdateStatusSale(id, atualizacaoStatusVenda.StatusDoPedido);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

    }
}