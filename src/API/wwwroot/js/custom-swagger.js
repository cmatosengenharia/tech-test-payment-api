document.addEventListener("DOMContentLoaded", function () {
    const checkExist = setInterval(function () {
        const descriptions = document.querySelectorAll('.opblock-summary-description');
        if (descriptions.length) {
            clearInterval(checkExist);
            descriptions.forEach(desc => {
                const parts = desc.innerHTML.split('-').slice(1);
                if (parts.length > 0) {
                    desc.innerHTML = '';
                    // Criar uma lista para organizar melhor os itens
                    const ul = document.createElement('ul');
                    parts.forEach(part => {
                        const li = document.createElement('li');
                        li.textContent = part.trim();
                        ul.appendChild(li);
                    });
                    desc.appendChild(ul);
                }
            });
        }
    }, 100);
});