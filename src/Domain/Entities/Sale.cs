﻿using Domain.Enum;

namespace Domain.Entities
{
    public class Sale
    {
        public int Id { get; set; }
        public Guid Identifier { get; set; }
        public int SellerId { get; set; }
        public DateTime Date { get; set; }
        public SaleStatus Status { get; set; }
        public List<SaleItem> Items { get; set; } = new List<SaleItem>();
        public virtual Seller Seller { get; set; } = new Seller();
    }
}