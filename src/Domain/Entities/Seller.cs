﻿namespace Domain.Entities
{
    public class Seller
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public virtual ICollection<Sale> Sales { get; set; } = new List<Sale>();
    }
}