﻿using Domain.Entities;

namespace Domain.Repositorys.Interfaces
{
    public interface ISellerRepository
    {
        Seller GetByIdAsync(int id);
        Seller AddAsync(Seller seller);
        void UpdateAsync(Seller seller);
        void DeleteAsync(int id);
    }
}