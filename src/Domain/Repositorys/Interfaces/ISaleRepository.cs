﻿using Domain.Entities;

namespace Domain.Repositorys.Interfaces
{
    public interface ISaleRepository
    {
        Sale GetByIdAsync(int id);
        Sale AddAsync(Sale sale);
        void UpdateAsync(Sale sale);
        void DeleteAsync(int id);
    }
}