﻿using System.ComponentModel;

namespace Domain.Enum
{
    public enum SaleStatus
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Enviado para Transportadora")]
        EnviadoParaTransportadora,
        [Description("Entregue")]
        Entregue,
        [Description("Cancelada")]
        Cancelada
    }
}