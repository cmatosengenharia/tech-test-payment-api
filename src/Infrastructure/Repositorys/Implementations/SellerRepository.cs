﻿using Domain.Entities;
using Domain.Repositorys.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Repositorys.Implementations
{
    public class SellerRepository : ISellerRepository
    {
        private readonly IMemoryCache _cache;

        public SellerRepository(IMemoryCache cache)
        {
            _cache = cache;
        }

        public Seller GetByIdAsync(int id)
        {
            _cache.TryGetValue("seller" + id, out Seller seller);
            return seller;
        }
        
        public Seller AddAsync(Seller seller)
        {
            _cache.Set("seller" + seller.Id, seller, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1) 
            });

            return seller;
        }

        public void UpdateAsync(Seller seller)
        {
            _cache.Set("seller" + seller.Id, seller, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
            });
        }

        public void DeleteAsync(int id)
        {
            _cache.Remove("seller" + id);
        }
    }
}