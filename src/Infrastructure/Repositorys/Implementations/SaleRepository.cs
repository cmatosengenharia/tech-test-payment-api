﻿using Domain.Entities;
using Domain.Repositorys.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Repositorys.Implementations
{
    public class SaleRepository : ISaleRepository
    {
        private readonly IMemoryCache _cache;
        private static int _nextId = 1;

        public SaleRepository(IMemoryCache cache)
        {
            _cache = cache;
        }

        public Sale GetByIdAsync(int id)
        {
            _cache.TryGetValue("sale" + id, out Sale sale);
            return sale;
        }

        public Sale AddAsync(Sale sale)
        {
            sale.Id = Interlocked.Increment(ref _nextId);

            _cache.Set("sale" + sale.Id, sale, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
            });

            return sale;
        }
        public void UpdateAsync(Sale sale)
        {
            _cache.Set("sale" + sale.Id, sale, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
            });
        }

        public void DeleteAsync(int id)
        {
            _cache.Remove("sale" + id);
        }
    }
}