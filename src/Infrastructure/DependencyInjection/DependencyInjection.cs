﻿using Application.Services.Implemetations;
using Application.Services.Implemetations.Validation;
using Application.Services.Interface;
using Application.Services.Interface.Validation;
using Domain.Repositorys.Interfaces;
using Infrastructure.Repositorys.Implementations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.DependencyInjection
{
    public static class DependencyInjection
    {
        public static void AddInfrastructureServices(this IServiceCollection services)
        {
            services.AddMemoryCache();

            #region Service

            services.AddScoped<ISalesService, SalesService>();
            services.AddScoped<ISaleValidationService, SaleValidationService>();

            #endregion

            #region Repository

            services.AddScoped<ISaleRepository, SaleRepository>();
            services.AddScoped<ISellerRepository, SellerRepository>();

            #endregion
        }
    }
}