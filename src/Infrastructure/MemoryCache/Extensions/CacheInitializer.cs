﻿using Domain.Entities;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.MemoryCache.Extensions
{
    public static class CacheInitializer
    {
        public static void InitializeSellerCache(this IMemoryCache cache)
        {
            var sellers = new List<Seller>
                          {
                              CreateSeller(1, "Geraldo", "geraldo1950@teste.com", "31 9 9999-4444"),
                              CreateSeller(2, "Joaquim", "joaquim1951@example.com", "31 9 9999-4445"),
                              CreateSeller(3, "Ferdinanda", "ferdinanda1960@example.com", "31 9 9999-4446")
                          };

            foreach (var seller in sellers)
            {
                cache.Set("seller" + seller.Id, seller, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(365)
                });
            }
        }

        public static Seller CreateSeller(int id, string name, string email, string phoneNumber)
        {
            return new Seller
            {
                Id = id,
                Name = name,
                Email = email,
                PhoneNumber = phoneNumber
            };
        }
    }
}